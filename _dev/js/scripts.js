import '../scss/app.scss'
import $ from 'jquery'
import '../bower_components/jquery-serialize-object/dist/jquery.serialize-object.min'
import { TweenMax } from 'gsap'
import './animations/loader'
let lobbyElem = $('#lobby')
let mapList = $('.maps')
const GoogleUrl = require('google-url')
let googleUrl = new GoogleUrl({key:'AIzaSyBi2_-9clU4UanbATZrPGfwnMQdHTC2hxI'})
const socket = io.connect(location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: ''),
{
    query:"lobbyId="+lobbyElem.data('lobby'),
    transports: ['websocket']
})
//JOIN ROOM
if(lobbyElem.length){
    socket.emit('room',lobbyElem.data('lobby'))
}

$(document).ready(function(){
    //Map type change
    let mapType = $('input[name="maps_type[]"]')
    if(mapType.length){
        mapType.on('change',function(){
            let typeSelected = $(this).val()
            if(typeSelected === 'all'){
                if($(this)[0].checked){
                    $('.form-check-type').prop('checked',true)
                }else {
                    $('.form-check-type').prop('checked',false)
                }
            }
            if($(this)[0].checked){
                $('.type-'+typeSelected).parent().find('input').prop('checked',true)
                if($('.form-check-type:not(#all):checked').length == $('.form-check-type').length){
                    $('#all').prop('checked',true)
                }
            }else {
                $('.type-'+typeSelected).parent().find('input').prop('checked',false)
                $('#all').prop('checked',false)
            }
        })
    }

    //info
    let infoElem = $('.info')
    if(infoElem.find('.container').html() != ''){
        TweenMax.to(infoElem,0.300,{
            height: 'auto',
            opacity: 1,
            top:'0',
            ease: Circ.easeOut
        })
    }

    //Links lobby
    let lobbyLinkInput = $('.lobby-links input')
    let lobbyLink = $('.lobby-links a')
    if(lobbyLink.length){
        lobbyLinkInput.click(function(){
            $(this).select()
        })
        lobbyLink.click(function(){
            $(this).next('input').select()
            document.execCommand( 'copy' )
            lobbyLink.html('copy')
            $(this).html('copied')
            return false;
        })
    }

    //Compress UI
    let compressButton = $('.compress-ui')
    let expandButton = $('.expand-ui')
    let mainContainer = $('#main-container')
    let colLobbySection = $('.lobby-section').parent()
    if(compressButton.length){
        expandButton.hide()
        compressButton.click(function(){
            mainContainer.removeClass('container-fluid').addClass('container')
            colLobbySection.removeClass('col-lg-6 col-lg-3')
            expandButton.show()
            $(this).hide()
        })
        expandButton.click(function(){
            mainContainer.removeClass('container').addClass('container-fluid')
            colLobbySection.eq(0).addClass('col-lg-3')
            colLobbySection.eq(1).addClass('col-lg-6')
            colLobbySection.eq(2).addClass('col-lg-3')
            compressButton.show()
            $(this).hide()
        })
    }

    //Green Background
    let greenButton = $('.background-green')
    greenButton.click(function(){
        $('body').toggleClass('green-background');
        $(this).toggleClass('active')
    })
    //Copy settings
    let copySettingsButton = $('#copySettings')
    function copyToClipboard(text){
        let dummy = document.createElement("input")
        document.body.appendChild(dummy)
        dummy.setAttribute('value', text)
        dummy.select()
        document.execCommand("copy")
        document.body.removeChild(dummy)
    }
    if(copySettingsButton.length){
        let messageTimeout = null
        copySettingsButton.click(function(){
            let formSettings = $('#createLobby').serialize()
            googleUrl.shorten( 'http://'+window.location.hostname+'?'+formSettings, function( err, shortUrl ) {
                let successMessage = $('.successCopied')
                let copySettingsField = $('#copy-settings')
                clearTimeout(messageTimeout)
                copySettingsField.val(shortUrl)
                TweenMax.to(copySettingsField,0.300,{
                    opacity: 1,
                    top:'0',
                    ease: Circ.easeIn
                })
                TweenMax.to(successMessage,0.300,{
                    opacity: 1,
                    top:'52px',
                    ease: Circ.easeOut,
                    onComplete: function(){
                        messageTimeout = setTimeout(function(){
                            TweenMax.to(successMessage,0.300,{
                                opacity: 0,
                                top:'-100px',
                                ease: Circ.easeIn
                            })
                        },5000)
                    }
                })
            } );
            return false;
        })
    }

    //READY STATUS
    let readyButton = $('.ready-button')
    if(readyButton.length){
        readyButton.click(function(){
            if(!$(this).hasClass('disabled')){
                let teamReady = $(this).data('team')
                socket.emit('teamReady',{
                    teamReady:teamReady,
                    lobbyId:$('#lobby').data('lobby')
                })
                $(this).html('READY')
            }
        })
    }

    socket.on('teamReady',function(team){
        $('.ready-button[data-team="'+team+'"]').addClass('disabled')
        $('.team'+team+'_status').removeClass('not-ready').addClass('ready').html('READY')
        $('.status-bar--team'+team+' h2 button').html('READY')
    })

    //UPDATE TIMER
    let timerSpan = $('.timer')
    socket.on('timerUpdate',function(seconds){
        timerSpan.html(seconds.seconds)
    })
    socket.on('startBan',function(team){
        startBan(team)
        $('.timer-info').html($('.status-bar--'+team).parent().find('h2').html())
    })
    socket.on('randomPick',function(team){
        randomPick(team)
    })
    socket.on('updateBans',function(values){
        let mapBan = mapList.find('li[data-name="'+values.chosen_map+'"]')
        
        //Desactivation de la map selection pour tous
        mapList.find('li').unbind('click')
        mapList.removeClass('enabled')

        //On place la map seléctionnée dans la liste de la team
        mapBan = mapBan.detach()
        mapBan.addClass('ban '+values.team)
        mapInBo(mapBan)
        mapBan.appendTo('.maps-'+values.team)

        TweenMax.fromTo(mapBan,0.190,{
            opacity:0,
            top: '-10px'
        },{
            opacity: 1,
            top:0,
            ease: Circ.easeOut
        })
    })
    socket.on('allTeamReady',function(){
        let lobbyInfosButton = $('.lobby-infos-button')
        if(lobbyInfosButton.hasClass('is-open')){
            lobbyInfosButton.trigger('click')
        }
        let status = $('.status-bar').children()
        TweenMax.fromTo(status,0.500,{
            top:0,
            opacity: 1,
        },{
            top:-20,
            opacity: 0,
            ease: Power1.easeOut,
            onComplete:function(){
                status.remove()
            }
        })
        $('.timer-info').html('Pre-draft')
    })
    socket.on('banFinish',function(team){
        let lastMapOrig = mapList.find('li:not(.ban)')
        let lastMap = lastMapOrig.clone()
        let lobbyStatus = $('.lobby-status span')
        let lobbyMessage = $('.lobby-message')
        let timerWrapper = $('.timer-wrapper')
        let mapOrder = lobbyElem.data('maporder')

        let type = 1
        switch (lobbyElem.data('type')) {
            case 'bo3':
                type = 3
                break
            case 'bo5':
                type = 5
                break
            case 'bo7':
                type = 7
                break
        }
        
        if(mapOrder == '321'){
            lastMapOrig.find('.map__type').html('<b>1</b>')
        }else {
            lastMapOrig.find('.map__type').html('<b>'+type+'</b>')
        }
        socket.emit('setLastMap',{
            lastMap: lastMap.data('name'),
            team: team
        })
        lastMapOrig.addClass(team)
        lastMap.addClass('in-bo')
        lastMap.appendTo('.maps-'+team)
        $('.timer').html('-')
        $('.timer-info').html('timer')
        lobbyStatus.html('Draft done')
        TweenMax.to(timerWrapper,0.500,{
            opacity: 0,
            top: -10,
            height: 0,
            ease: Circ.easeIn,
            onComplete: function(){
                timerWrapper.remove()
            }
        })
        TweenMax.to(lobbyMessage,0.190,{
            opacity: 1,
            top:0,
            margin: '15px 0',
            ease: Circ.easeOut
        })
    })
    socket.on('setSelectedMaps',function(bans){
        let bansMap = bans.split(',')
        let mapOrder = lobbyElem.data('maporder')
        let position = 1
        bansMap.reverse()

        let type = 1
        switch (lobbyElem.data('type')) {
            case 'bo3':
                type = 3
                break
            case 'bo5':
                type = 5
                break
            case 'bo7':
                type = 7
                break
        }
        bansMap.forEach(function(map, index) {
            if(index > 0 && type > index){
                if(mapOrder=='321'){
                    position = parseInt(index+1)
                }else {
                    position = type - index
                }

                let mapSelected = $('.map-list').find('li[data-name="'+map+'"]').clone()
                mapSelected.removeClass('ban in-bo')
                mapSelected.find('.map__type').html('<b>'+position+'</b>')
                if(mapOrder=='321'){
                    mapSelected.appendTo('.maps')
                }else {
                    mapSelected.prependTo('.maps')
                }
                TweenMax.fromTo(mapSelected,0.190,{
                    opacity:0,
                    top: '-10px'
                },{
                    opacity: 1,
                    top:0,
                    ease: Circ.easeOut
                })
            }else {
                return false
            }
        });
        mapList.addClass('done')
    })
})
function randomPick(team){
    let currentTeam = getCurrentTeam()
    if(team == currentTeam){
        let mapNumber = mapList.find('li:not(.ban)').length
        mapNumber = Math.floor((Math.random() * mapNumber))
        pickMap(mapList.find('li:not(.ban)').eq(mapNumber),team,true)
    }
}
function mapInBo(map){
    let nbMaps = mapList.find('li:not(.ban)').length + 1
    let boType = 1
    switch (lobbyElem.data('type')) {
        case 'bo3':
            boType = 3
            break
        case 'bo5':
            boType = 5
            break
        case 'bo7':
            boType = 7
            break
    }
    if(nbMaps <= boType){
        map.addClass('in-bo')
    }
}
function startBan(team){
    let currentTeam = getCurrentTeam()
    $('.timer').removeClass('team1-timer team2-timer').addClass(team+'-timer')
    updateLobbyStatus()
    if(team == currentTeam){
        mapList.addClass('enabled')
        mapList.find('li').click(function(){
            if($(this).hasClass('ban')){
                return false
            }
            pickMap($(this),team,false)
        })
    }
}
function updateLobbyStatus(){
    let nbMaps = mapList.find('li:not(.ban)').length
    let mapOrder = lobbyElem.data('maporder')
    let boType = 1
    switch (lobbyElem.data('type')) {
        case 'bo3':
            boType = 3
            break
        case 'bo5':
            boType = 5
            break
        case 'bo7':
            boType = 7
            break
    }
    if(nbMaps <= boType){
        let messageStatus = '';
        let lobbyStatus = $('.lobby-status span')

        if(mapOrder == '321'){
            switch (nbMaps){
                case 2:
                    messageStatus = 'PICK 2nd MAP'
                    break
                case 3:
                    messageStatus = 'PICK 3rd MAP'
                    break
                default:
                    messageStatus = 'PICK '+nbMaps+'th MAP'
                    break
            }
        }else {
            let currentPick = parseInt(boType)-(parseInt(nbMaps)-1)
            switch (true){
                case boType == nbMaps:
                    messageStatus = 'PICK 1st MAP'
                    break
                case currentPick == 2:
                    messageStatus = 'PICK 2nd MAP'
                    break
                case currentPick == 3:
                    messageStatus = 'PICK 3rd MAP'
                    break
                default:
                    messageStatus = 'PICK '+currentPick+'th MAP'
                    break
            }
        }
        lobbyStatus.html(messageStatus)
    }
}
function pickMap(mapElem,team,random){
    let nbMaps = mapList.find('li:not(.ban)').length - 1
    socket.emit('nextTeamRound',{
        maps: nbMaps,
        chosen_map: mapElem.data('name'),
        team: team,
        random: random
    })
    mapInBo(mapElem)
}
function getCurrentTeam(){
    let lobbyElem = $('#lobby')
    let tokenTeam1 = lobbyElem.data('team1')
    let tokenTeam2 = lobbyElem.data('team2')
    let currentToken = lobbyElem.data('token')
    if(tokenTeam1 == currentToken){
        return 'team1'
    }
    if(tokenTeam2 == currentToken){
        return 'team2'
    }
    return false
}
