import $ from 'jquery'
import { TweenMax } from 'gsap'

let showUp = (elem) => {
    TweenMax.to(elem,0.500,{
        y: 0,
        opacity: 1,
        ease: Expo.easeOut
    })
}
let hideUp = (elem) => {
    TweenMax.to(elem, 0.500, {
        y: -100,
        height: 0,
        opacity: 0,
        ease: Expo.easeOut
    })
}

//Advanced settings
let advancedSettingsButton = $('.advanced-settings')
if(advancedSettingsButton.length){
    let advancedSettings = $('.advanced-settings-content')
    TweenMax.set(advancedSettings.find('.row'),{
        y: -100,
        height:0,
        opacity: 0
    })
    let openCloseSettings = () => {
        if(advancedSettings.hasClass('is-open')){
            TweenMax.set(advancedSettings.find('.row'),{
                height:'auto'
            })
            TweenMax.from(advancedSettings.find('.row'),0.500,{
                height: 0,
                ease: Expo.easeOut
            })
            showUp(advancedSettings.find('.row'))
        }else {
            hideUp(advancedSettings.find('.row'))
        }
    }
    advancedSettingsButton.click(function(){
        advancedSettingsButton.toggleClass('is-open')
        advancedSettings.toggleClass('is-open')
        openCloseSettings()
    })
    openCloseSettings()
}