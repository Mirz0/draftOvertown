import $ from 'jquery'
import { TweenMax } from 'gsap'

let showUp = (elem, cb) => {
    TweenMax.to(elem,0.500,{
        top:0,
        opacity: 1,
        ease: Power1.easeOut,
        onComplete: ()=>{
            cb()
        }
    })
}

showUp($('.main-container'),()=>{
    let errorMessage = $('.errorMessage')
    if(errorMessage.length){
        showUp(errorMessage.find('span'),()=>{})
    }
})