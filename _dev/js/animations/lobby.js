//TweenLite.set($content, {height:"auto"})
//TweenLite.from($content, 0.2, {height:0})
import $ from 'jquery'
import { TweenMax } from 'gsap'

let lobbyStatus = $('#lobby').data('status')

let lobbyInfoButton = $('.lobby-infos-button')
let lobbyInfoContent = $('.lobby-infos-content')
if(lobbyInfoButton.length){
    if(lobbyInfoContent.hasClass('is-open')){
        TweenMax.set(lobbyInfoContent,{
            height:'auto'
        })
        TweenMax.from(lobbyInfoContent,0.300,{
            height: 15,
            ease: Circ.easeOut
        })
        TweenMax.to(lobbyInfoContent.find('.row'),0.200,{
            opacity:1,
            top:0,
            delay: 0.100,
            ease: Power1.easeOut
        })
    }
    lobbyInfoButton.click(function(){
        $(this).toggleClass('is-open')
        lobbyInfoContent.toggleClass('is-open')

        if($(this).hasClass('is-open')){
            TweenMax.set(lobbyInfoContent,{
                height:'auto'
            })
            TweenMax.from(lobbyInfoContent,0.300,{
                height: 15,
                ease: Circ.easeOut
            })
            TweenMax.to(lobbyInfoContent.find('.row'),0.200,{
                opacity:1,
                top:0,
                delay: 0.100,
                ease: Power1.easeOut
            })
        }else {
            TweenMax.to(lobbyInfoContent,0.190,{
                height:15,
                ease: Circ.easeOut
            })
            TweenMax.to(lobbyInfoContent.find('.row'),0.200,{
                opacity:0,
                top:30,
                ease: Power1.easeOut
            })
        }

        return false
    })
}

let maps = $('.map-list').find('li')
if(maps.length){
    if(lobbyStatus=='0'){
        TweenMax.staggerFromTo(maps, 0.300,
            {
                opacity:0,
                top:-10
            },
            {
                opacity:1,
                top:0,
                ease: Expo.easeOut
            },
            0.1
        )
    }else {
        TweenMax.fromTo(maps, 0.500,
            {
                opacity:0,
                top:-10
            },
            {
                opacity:1,
                top:0,
                ease: Expo.easeOut
            }
        )
    }
}

let status = $('.status-bar')
let $lobbyStatus = $('.lobby-status span')
if(status.length){
    TweenMax.to(status,0.800,{
        opacity:1,
        width:'100%',
        ease: Circ.easeOut,
        onComplete: function(){
            TweenMax.to($lobbyStatus,0.500,{
                top:0,
                ease: Circ.easeOut
            })
            if(lobbyStatus=='0'){
                TweenMax.to(status.children(),0.500,{
                    top:0,
                    ease: Circ.easeOut
                })
            }
        }
    })
}