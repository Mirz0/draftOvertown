let path = require("path");
let webpack = require("webpack");
let ExtractTextPlugin = require("extract-text-webpack-plugin");
let styleLintPlugin = require("stylelint-webpack-plugin");
const Uglify = require("uglifyjs-webpack-plugin");

require("es6-promise").polyfill();

module.exports = {
    entry: "./js/scripts.js",
    node: {
        fs: "empty",
        net: 'empty',
        tls: 'empty',
        dns: 'empty'
    },
    output: {
        path: __dirname,
        filename: "../dist/assets/js/scripts.js"
    },

    plugins: [
        // Specify the resulting CSS filename
        new ExtractTextPlugin("../dist/assets/css/app.css"),
        new Uglify()
    ],

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules/, /bower_components/],
                use: ["babel-loader"]
            },
            {
                test: /\.scss$/,
                exclude: [/node_modules/, /bower_components/],
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ["css-loader", "postcss-loader", "sass-loader"]
                })
            },
            {
                test: /\.(ttf|eot|woff|woff2)$/,
                loader: "file-loader",
                options: {
                    name: "font/[name].[ext]"
                }
            }
        ]
    },

    stats: {
        // Colored output
        colors: true
    },

    // Create Sourcemaps for the bundle
    devtool: "source-map"
};
