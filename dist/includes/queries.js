const crypto = require("crypto");
const assert = require("assert");
const mongo = require("mongodb");
const dotenv = require("dotenv").config();
const MongoClient = mongo.MongoClient;
const ObjectId = mongo.ObjectID;
const url = dotenv.parsed.DB_URL;
let dbDraft;

function collectionExists(name, cb) {
    dbDraft.listCollections().toArray(function(err, collections) {
        if (err) return cb(err);

        cb(
            null,
            collections.some(function(coll) {
                return coll.name == name;
            })
        );
    });
}

function connectDB(cb) {
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        console.log("Connected successfully to MongoDB");
        dbDraft = db.db(dotenv.parsed.DB_NAME);
        cb(db);
        collectionExists("maps", function(err, res) {
            if (!res) {
                dbDraft.collection("maps").insertMany([
                    { name: "Hanamura", slug: "hanamura", type: "assault" },
                    {
                        name: "Horizon Lunar Colony",
                        slug: "horizon",
                        type: "assault"
                    },
                    {
                        name: "Temple of Anubis",
                        slug: "anubis",
                        type: "assault"
                    },
                    {
                        name: "Volskaya Industries",
                        slug: "volskaya",
                        type: "assault"
                    },
                    { name: "Dorado", slug: "dorado", type: "escort" },
                    {
                        name: "Junkertown",
                        slug: "junkertown",
                        type: "escort"
                    },
                    { name: "Route 66", slug: "route66", type: "escort" },
                    {
                        name: "Watchpoint: Gibraltar",
                        slug: "gibraltar",
                        type: "escort"
                    },
                    {
                        name: "Rialto",
                        slug: "rialto",
                        type: "escort"
                    },
                    {
                        name: "Blizzard World",
                        slug: "blizzardworld",
                        type: "hybrid"
                    },
                    {
                        name: "Eichenwalde",
                        slug: "eichenwalde",
                        type: "hybrid"
                    },
                    {
                        name: "Hollywood",
                        slug: "hollywood",
                        type: "hybrid"
                    },
                    {
                        name: "King's Row",
                        slug: "kingsrow",
                        type: "hybrid"
                    },
                    { name: "Numbani", slug: "numbani", type: "hybrid" },
                    { name: "Ilios", slug: "ilios", type: "control" },
                    {
                        name: "Lijiang Tower",
                        slug: "lijiang",
                        type: "control"
                    },
                    { name: "Nepal", slug: "nepal", type: "control" },
                    { name: "Oasis", slug: "oasis", type: "control" },
                    { name: "Busan", slug: "busan", type: "control" },
                    { name: "Ayutthaya", slug: "ayutthaya", type: "arena" },
                    {
                        name: "Black Forest",
                        slug: "blackforest",
                        type: "arena"
                    },
                    { name: "Castillo", slug: "castillo", type: "arena" },
                    {
                        name: "Château Guillard",
                        slug: "chateau",
                        type: "arena"
                    },
                    {
                        name: "Ecopoint: Antarctica",
                        slug: "antarctica",
                        type: "arena"
                    },
                    {
                        name: "Necropolis",
                        slug: "necropolis",
                        type: "arena"
                    }
                ]);
            }
        });
    });
}
module.exports.connectDB = connectDB;

function createLobby(values, cb) {
    let dateNow = new Date();
    if (!values.maps_list) {
        values.maps_list = "";
    }
    if (!values.maps_type) {
        values.maps_type = "";
    }
    if (!values.startingTeam) {
        values.startingTeam = "team1";
    }
    if (!values.mapOrder) {
        values.mapOrder = "321";
    }
    if (!values.roundTimer) {
        values.roundTimer = 30;
    } else {
        values.roundTimer = parseInt(values.roundTimer);
    }
    if (!values.lobbyName) {
        values.lobbyName = values.team1 + " VS " + values.team2;
    }

    dbDraft.collection("lobbies").insertOne(
        {
            name: values.lobbyName,
            team1: values.team1,
            team2: values.team2,
            type: values.type,
            ready: 0,
            current_team: "",
            bans: "",
            bans_team1: "",
            bans_team2: "",
            mapOrder: values.mapOrder,
            status: 0,
            roundTimer: values.roundTimer,
            startingTeam: values.startingTeam,
            maps_type: values.maps_type,
            maps_list: values.maps_list,
            token_team1: crypto
                .createHash("sha256")
                .update(
                    "team1" +
                        values.lobbyName +
                        values.team1 +
                        values.team2 +
                        dateNow.getTime()
                )
                .digest("hex"),
            token_team2: crypto
                .createHash("sha256")
                .update(
                    "team2" +
                        values.lobbyName +
                        values.team1 +
                        values.team2 +
                        dateNow.getTime()
                )
                .digest("hex"),
            token_observer: crypto
                .createHash("sha256")
                .update(
                    "observer" +
                        values.lobbyName +
                        values.team1 +
                        values.team2 +
                        dateNow.getTime()
                )
                .digest("hex"),
            created_at: dateNow
        },
        function(err, res) {
            if (err) {
                console.log("Error Create Lobby :");
                console.log(err);
                cb(values, true);
            } else {
                if (cb != null) {
                    cb(res.insertedId, false);
                }
            }
        }
    );
}
module.exports.createLobby = createLobby;

function getMaps(maps, order, cb) {
    let maplist = {};
    let orderMap = { type: 1, name: 1 };

    /*
    if(order){
        orderMap = {[order]:1}
    }
    */

    if (maps) {
        let ids = [];
        for (i = 0; i < maps.length; i++) {
            let mapId = maps[i].toString();
            if (ObjectId.isValid(mapId)) {
                mapId = new ObjectId(mapId);
                ids.push(mapId);
            }
        }

        maplist = {
            _id: {
                $in: ids
            }
        };
    }

    dbDraft
        .collection("maps")
        .find(maplist)
        .sort(orderMap)
        .toArray(function(err, result) {
            if (result) {
                for (i = 0; i < result.length; i++) {
                    result[i]._id = result[i]._id.toString();
                }
                cb(result);
            } else {
                console.log("Error Get Maps : ");
                console.log(result);
                cb(false);
            }
        });
}
module.exports.getMaps = getMaps;

function getLobby(id, cb) {
    id = id.toString();
    if (ObjectId.isValid(id)) {
        id = new ObjectId(id);
    }
    dbDraft
        .collection("lobbies")
        .findOne({ _id: id }, {}, function(err, lobby) {
            if (err) {
                console.log("Error Get Lobby : " + id);
                console.log(err);
            } else {
                cb(lobby);
            }
        });
}
module.exports.getLobby = getLobby;

function getLobbyField(id, field, cb) {
    id = id.toString();
    if (ObjectId.isValid(id)) {
        id = new ObjectId(id);
    }
    dbDraft
        .collection("lobbies")
        .findOne({ _id: id }, {}, function(err, lobby_field) {
            if (err) {
                console.log("Error Get Lobby field : " + field);
                console.log(err);
            } else {
                cb(lobby_field[field]);
            }
        });
}
module.exports.getLobbyField = getLobbyField;

function updateLobby(id, champs, values, cb) {
    if (champs.isArray) {
    } else {
        id = id.toString();
        if (ObjectId.isValid(id)) {
            id = new ObjectId(id);
        }
        dbDraft
            .collection("lobbies")
            .findOneAndUpdate(
                { _id: id },
                { $set: { [champs]: values } },
                { returnOriginal: false },
                function(err, result) {
                    if (err) {
                        console.log("Error updateLobby : " + id);
                        console.log("Field : " + champs);
                        console.log("Value : " + values);
                        console.log(err);
                        cb(false);
                    } else {
                        if (cb) {
                            cb(result);
                        }
                    }
                }
            );
    }
}
module.exports.updateLobby = updateLobby;
