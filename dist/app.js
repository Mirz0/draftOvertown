const express = require("express")
const app = express()
const server = require("http").createServer(app)
const io = require("socket.io").listen(server)
const eSession = require("express-session")
const MongoStore = require('connect-mongo')(eSession);
const sharedSession = require("express-socket.io-session")
const bodyParser = require("body-parser")
const timer = require("easytimer.js")
const { check, validationResult } = require("express-validator/check")
const { matchedData, sanitize } = require("express-validator/filter")
const queries = require("./includes/queries")
const favicon = require("serve-favicon")
const path = require("path")
const dotenv = require('dotenv').config()
const RateLimit = require("express-rate-limit")
if(dotenv.parsed.ENVIRONEMENT == 'test'){
    //const auth = require("./includes/auth")
}

//REQUEST LIMITER (prevent DDOS and brute force)
app.enable("trust proxy")
let getLimiter = new RateLimit({
    windowMs: 60 * 1000, // 1 minute
    max: 200,
    delayMs: 0 // disabled
})
let postLimiter = new RateLimit({
    windowMs: 5 * 60 * 1000, // 5 minutes
    max: 10,
    delayAfter: 1,
    delayMs: 3 * 1000
})

//Globals
let roomVars = {}

//SESSION
const session = eSession({
    store: new MongoStore({
        url: dotenv.parsed.DB_URL,
        ttl: 24 * 60 * 60 //1 day
    }),
    resave: true,
    saveUninitialized: true,
    secret: 'overtowndraft2018-13252405'
})

//CONNECT TO MONGODB
queries.connectDB(function(db) {
    //VIEW ENGINE
    app.set("view engine", "twig")

    //USES
    //app.use(auth)
    app.use(bodyParser.json())
    app.use(
        bodyParser.urlencoded({
            // to support URL-encoded bodies
            extended: true
        })
    )
    app.use(session)
    io.use(
        sharedSession(session)
    )
    app.use(express.static("assets"))
    app.use(favicon(path.join(__dirname, "assets", "img", "favicon.ico")))
    app.use("/", getLimiter)
    app.use("/lobby/:name/:id/:token", getLimiter)

    //ROUTES
    app.get("/", function(req, res) {
        if(dotenv.parsed.MAINTENANCE=='true'){
            res.render(__dirname + "/views/maintenance", {
                info: dotenv.parsed.INFO,
                title: "OverTown Draft - Maintenance"
            })
        }
        if(dotenv.parsed.MAINTENANCE=='coming'){
            res.render(__dirname + "/views/comingsoon", {
                info: dotenv.parsed.INFO,
                title: "OverTown Draft - Coming soon"
            })
        }
        //Affichage du formulaire de création d'un lobby
        queries.getMaps(null, "type", function(maps) {
            if (maps) {
                res.render(__dirname + "/views/home", {
                    info: dotenv.parsed.INFO,
                    title: "OverTown Draft - Create Lobby",
                    maps: maps,
                    form: req.query
                })
            } else {
                res.send("ERROR PLEASE TRY AGAIN LATER ¯\\_(ツ)_/¯")
            }
        })
    })
    app.post("/", postLimiter, (req, res, next) => {
        //A la soumission du formulaire de création d'un lobby on vérifie les données
        const errors = validationResult(req)
        let nbMaps = 0
        if (req.body.maps_list) {
            nbMaps = req.body.maps_list.length
        }
        let boType = req.body.type.split("bo")
        boType = boType[1]
        let correctFields = "true"
        if (boType > nbMaps) {
            correctFields = "You have to select more map than BO type."
        }
        if (!req.body.roundTimer) {
            req.body.roundTimer = 30
        }
        if (req.body.startingTeam == 'random') {
            let randomTeam = ['team1','team2']
            req.body.startingTeam = randomTeam[Math.round(Math.random())]
        }
        if (req.body.roundTimer % 1 !== 0) {
            correctFields = "Round Timer value is incorrect."
        } else {
            if (req.body.roundTimer < 5 || req.body.roundTimer > 300) {
                correctFields =
                    "Round Timer value must be greater than 5s and can't exceed 300s."
            }
        }
        if (!errors.isEmpty() || correctFields != "true") {
            //Si il y a des erreurs on les renvoient dans la vue
            queries.getMaps(null, "type", function(maps) {
                if (maps) {
                    if (!errors.isEmpty()) {
                        res.render(__dirname + "/views/home", {
                            info: dotenv.parsed.INFO,
                            title: "OverTown Draft - Create Lobby",
                            errors: errors.mapped(),
                            form: req.body,
                            maps: maps
                        })
                    }
                    if (correctFields != "true") {
                        res.render(__dirname + "/views/home", {
                            info: dotenv.parsed.INFO,
                            title: "OverTown Draft - Create Lobby",
                            errorMessage: correctFields,
                            form: req.body,
                            maps: maps
                        })
                    }
                } else {
                    res.send("ERROR PLEASE TRY AGAIN LATER ¯\\_(ツ)_/¯")
                }
            })
        } else {
            //Si il n'y a pas d'erreur on lance la création du lobby
            queries.createLobby(req.body, function(lobbyId, fail) {
                if (!fail) {
                    //Si la création s'est bien passé on redirige vers la vue du lobby créé
                    queries.getLobby(lobbyId, function(results) {
                        if (results) {
                            res.redirect(`/lobby/${encodeURIComponent(results.name)}/${results._id}/${results.token_team1}`)
                        } else {
                            res.render(__dirname + "/views/home", {
                                info: dotenv.parsed.INFO,
                                title: "OverTown Draft - Create Lobby",
                                fail: true,
                                form: lobbyId
                            })
                        }
                    })
                } else {
                    //Si la création a échouée on affiche un message d'erreur sur le formulaire
                    res.render(__dirname + "/views/home", {
                        info: dotenv.parsed.INFO,
                        title: "OverTown Draft - Create Lobby",
                        fail: true,
                        form: lobbyId
                    })
                }
            })
        }
    })
    app.get("/lobby/:name/:id/:token", function(req, res) {
        //Récupération du lobby appelé
        queries.getLobby(req.params.id, function(results) {
            if (results) {
                //On envoie la vue du lobby avec les maps choisies et le token de l'utilisateur
                queries.getMaps(results.maps_list, false, function(maps) {
                    if (maps) {
                        results.created_at = results.created_at.toLocaleString(
                            "us-US",
                            { timeZone: "Europe/Paris" }
                        )
                        res.render(__dirname + "/views/lobby", {
                            info: dotenv.parsed.INFO,
                            title: "OverTown Draft",
                            lobby: results,
                            maps: maps,
                            baseUrl: req.protocol + "://draft.overtown.fr",
                            token: req.params.token
                        })
                    } else {
                        res.send("ERROR PLEASE TRY AGAIN LATER ¯\\_(ツ)_/¯")
                    }
                })
            } else {
                //Aucun lobby trouvé ...
                res.render(__dirname + "/views/unkown_lobby.twig", {
                    info: dotenv.parsed.INFO,
                    title: "OverTown Draft - Lobby not found"
                })
            }
        })
    })

    //SOCKETS
    io.on("connection", function(socket) {
        socket.on("room", function(room) {
            roomInit(room)

            //Récupération de la team en train de ban si déjà en cours
            queries.getLobbyField(room, "current_team", function(current_team) {
                if (current_team) {
                    io.to(room).emit("startBan", current_team)
                }
            })
        })
        socket.on("disconnect", function() {})
        socket.on("teamReady", function(values) {
            roomInit(values.lobbyId)
            
            //Récupération du statut ready
            queries.getLobbyField(values.lobbyId, "ready", function(ready) {
                let readyValue
                let startingTeam = roomVars[values.lobbyId].startingTeam
                if (startingTeam !== "team1" && startingTeam !== "team2") {
                    startingTeam = "team1"
                }

                //Check le statut ready (1: team1 ready / 2: team2 ready / 3: les deux ready)
                if (ready === 0) {
                    readyValue = values.teamReady
                } else {
                    readyValue = 3
                }
                //Mise à jour du status ready
                queries.updateLobby(
                    values.lobbyId,
                    "ready",
                    readyValue,
                    function(result) {
                        if (result) {
                            io
                                .to(socket.handshake.session.room)
                                .emit("teamReady", values.teamReady)
                            //Si le statut ready est 3 les deux teams sont prêtes, on lance le timer pré-round
                            if (readyValue === 3) {
                                //On informe le front que toute les team sont prêtes
                                io
                                    .to(socket.handshake.session.room)
                                    .emit("allTeamReady")
                                queries.updateLobby(
                                    socket.handshake.session.room,
                                    "status",
                                    1
                                )
                                if (
                                    !roomVars[socket.handshake.session.room]
                                        .beginTimer
                                ) {
                                    roomVars[
                                        socket.handshake.session.room
                                    ].beginTimer = new timer()
                                }
                                timerStart(
                                    socket.handshake.session.room,
                                    roomVars[socket.handshake.session.room]
                                        .beginTimer,
                                    10,
                                    function() {
                                        //A la fin du timer pré-round on lance le premier round de ban pour la première team
                                        startRound(startingTeam)
                                    }
                                )
                            }
                        } else {
                            //Impossible de mettre à jour le statut ready ... ?
                            console.log("Ready error")
                        }
                    }
                )
            })
        })
        socket.on("nextTeamRound", function(values) {
            if (values.random) {
                if (!roomVars[socket.handshake.session.room].randomPick) {
                    roomVars[socket.handshake.session.room].randomPick = true
                } else {
                    return false
                }
            }

            //On récupère les bans de la team pour ajouter la nouvelle map ban
            queries.getLobbyField(
                socket.handshake.session.room,
                "bans_" + values.team,
                function(bans_team) {
                    if (bans_team !== "") {
                        let bans_team_array = bans_team.split(',')
                        if(bans_team_array.includes(values.chosen_map)){
                            return false
                        }else {
                            bans_team += "," + values.chosen_map
                        }
                    } else {
                        bans_team = values.chosen_map
                    }
                    //Mis à jour de la liste des maps ban pour la team
                    queries.updateLobby(
                        socket.handshake.session.room,
                        "bans_" + values.team,
                        bans_team
                    )
                }
            )

            //On récupère les bans globaux pour ajouter la nouvelle map ban
            queries.getLobbyField(
                socket.handshake.session.room,
                "bans",
                function(bans) {
                    if (bans !== "") {
                        let bans_array = bans.split(',')
                        if(bans_array.includes(values.chosen_map)){
                            return false
                        }else {
                            bans += "," + values.chosen_map
                        }
                    } else {
                        bans = values.chosen_map
                    }
                    //Mis à jour de la liste des maps ban
                    queries.updateLobby(
                        socket.handshake.session.room,
                        "bans",
                        bans
                    )
                }
            )

            //On informe le client de l'ajout de la nouvelle map dans les listes
            io.to(socket.handshake.session.room).emit("updateBans", {
                chosen_map: values.chosen_map,
                team: values.team
            })

            //On passe au round suivant
            nexTeamRound(values.maps, values.team)
        })
        socket.on("setLastMap", function(values) {
            if (!roomVars[socket.handshake.session.room].setLastMap) {
                roomVars[socket.handshake.session.room].setLastMap = true

                queries.getLobbyField(
                    socket.handshake.session.room,
                    "bans_" + values.team,
                    function(bans_team) {
                        if (bans_team !== "") {
                            bans_team += "," + values.lastMap
                        } else {
                            bans_team = values.lastMap
                        }
                        //Mis à jour de la liste des maps ban pour la team
                        queries.updateLobby(
                            socket.handshake.session.room,
                            "bans_" + values.team,
                            bans_team
                        )
                    }
                )

                //On récupère les maps bans par les deux équipes et on ajoute la dernière map
                queries.getLobbyField(
                    socket.handshake.session.room,
                    "bans",
                    function(bans) {
                        if (bans !== "") {
                            bans += "," + values.lastMap
                        } else {
                            bans = values.lastMap
                        }
                        //Mise à jour de la liste des maps ban
                        queries.updateLobby(
                            socket.handshake.session.room,
                            "bans",
                            bans
                        )
                        //Mise à jour du status à 2 (bans terminés)
                        queries.updateLobby(
                            socket.handshake.session.room,
                            "status",
                            2
                        )
                        //On envoi au client la liste finale des maps bans
                        io
                            .to(socket.handshake.session.room)
                            .emit("setSelectedMaps", bans)
                    }
                )
            }
        })

        function roomInit(room){
            socket.join(room)

            //Ajout de l'ID de la room en session
            socket.handshake.session.room = room
            
            //Enregistrement des variables de session
            socket.handshake.session.save()
            
            //Initialisation de la room
            if (!roomVars[room]) {
                roomVars[room] = {}

                if (!roomVars[room].beginTimer) {
                    roomVars[room].beginTimer = new timer()
                }
                if (!roomVars[room].roundTimer) {
                    roomVars[room].roundTimer = new timer()
                }

                //Récupération du roundTimer
                queries.getLobbyField(room, "roundTimer", function(
                    roundTimerValue
                ) {
                    if (!roomVars[room].roundTimerValue) {
                        roomVars[room].roundTimerValue = roundTimerValue
                    }
                })

                //Récupération de la startingTeam
                queries.getLobbyField(room, "startingTeam", function(startingTeam) {
                    if (!roomVars[room].startingTeam) {
                        roomVars[room].startingTeam = startingTeam
                    }
                })
            }
        }
        function startRound(team) {
            //On informe le client du démarrage du round
            io.to(socket.handshake.session.room).emit("startBan", team)
            //On met à jour la team qui est en cours de ban
            queries.updateLobby(
                socket.handshake.session.room,
                "current_team",
                team
            )

            //On lance le timer du round
            if (!roomVars[socket.handshake.session.room].roundTimer) {
                roomVars[
                    socket.handshake.session.room
                ].roundTimer = new timer()
            }
            timerStart(
                socket.handshake.session.room,
                roomVars[socket.handshake.session.room].roundTimer,
                roomVars[socket.handshake.session.room].roundTimerValue,
                function() {
                    //Si on arrive à la fin du timer on demande au client de faire un random pick
                    roomVars[socket.handshake.session.room].randomPick = null
                    io
                        .to(socket.handshake.session.room)
                        .emit("randomPick", team)
                }
            )
        }
        function nexTeamRound(maps, team) {
            //On change la team
            if (team == "team1") {
                team = "team2"
            } else {
                team = "team1"
            }
            if (maps > 1) {
                //Si il reste encore des maps dans la liste on reset le timer du round pour le prochain
                if(roomVars[socket.handshake.session.room].roundTimer){
                    roomVars[socket.handshake.session.room].roundTimer.stop()
                }
                roomVars[
                    socket.handshake.session.room
                ].roundTimer = new timer()

                //on start le nouveau round
                startRound(team)
            } else {
                //Si il rest qu'une map dans la liste on reset le timer du round
                if(roomVars[socket.handshake.session.room].roundTimer){
                    roomVars[socket.handshake.session.room].roundTimer.stop()
                }
                roomVars[
                    socket.handshake.session.room
                ].roundTimer = new timer()

                //On vide le champ de la team en cours de ban
                queries.updateLobby(
                    socket.handshake.session.room,
                    "current_team",
                    ""
                )

                //Si la fonction est appelée la première fois (donc il reste des timers dans la room)
                if (roomVars[socket.handshake.session.room].roundTimer) {
                    //On les supprime et on informe le client que c'est la fin du ban
                    delete roomVars[socket.handshake.session.room].roundTimer
                    delete roomVars[socket.handshake.session.room].beginTimer
                    io
                        .to(socket.handshake.session.room)
                        .emit("banFinish", team)
                }
            }
        }
        function timerStart(room, timerRound, time, cb) {
            //On lance le timer concerné en compte à rebours
            timerRound.start({
                countdown: true,
                startValues: {
                    seconds: time
                }
            })

            //On informe le client que le timer est lancé
            io.to(room).emit("timerUpdate", timerRound.getTimeValues())

            //Pour chaque seconde du timer on informe le client que le timer à changé
            timerRound.addEventListener("secondsUpdated", function() {
                io.to(room).emit("timerUpdate", timerRound.getTimeValues())
            })

            if (cb != null) {
                //On execute la fonction callback lorsque le timer est terminé
                timerRound.addEventListener("targetAchieved", cb)
            }
        }
    })

    //Handle 404
    app.use(function(req, res, next) {
        res.status(404)
        res.render(__dirname + "/views/404", {
            title: "OverTown Draft - Page not found"
        })
    })

    //START SERVER
    server.listen(7000)

    //ON APP STOP
    process.on("SIGINT", function() {
        db.close(function() {
            process.exit(0)
        })
    })
})
