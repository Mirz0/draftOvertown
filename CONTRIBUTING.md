# Visualiser les données

1. Télécharger [MongoDB Compass](https://www.mongodb.com/download-center/compass?filter=enterprise&utm_source=google&utm_campaign=EMEA_France_CorpEntOnly_Brand_Alpha_FM&utm_keyword=mongodb%20compass&utm_device=c&utm_network=g&utm_medium=cpc&utm_creative=208952627410&utm_matchtype=e&_bt=208952627410&_bk=mongodb%20compass&_bm=e&_bn=g&jmp=search&gclid=EAIaIQobChMIiKeRroac3gIVBPhRCh209wq1EAAYASAAEgIshfD_BwE)
2. Créer une nouvelle connexion à un hôte
3. Mettre les informations de l'hebergeur, SSH tunnel "Use identity file", port 2121, fichier id_rsa
4. Ouvrir la database "overtown-fr"

# Ajouter une map

Ouvrir la collection "maps" dans compass et inserer un nouveau document en reprenant les mêmes
champs que les maps existantes.

Ajouter aussi la nouvelle map dans le fichier `dist/includes/queries.js`(au cas où la bdd devait être recréée).